<?php

/*
items array is [$itemId => $neededQuantity]
This array has the list of items and quantities which I need to buy.
*/
$items = require_once('items.php');

/*
boxes array is $boxId => [$itemId => $quantityInBox]
This array has the catalog of boxes available to buy.
*/
$boxes = require_once('boxes.php');

/*
We have boxes which have N items in it with N quantity. Items can be in 1 or more boxes.
How many boxes from which box do I need to buy, to satisfy the needed item quantities?
The provided data should give back: needed box quantity and which items are in the box.
What would be the best combination to have least amount of boxes.
An OOP approach would add more point to the test.
*/

class RequiredBoxes {
	private $_items;

	private $_catalog;

	private $_evaluatedCatalog;

	private $_requiredBoxes;

	private function _evaluateCatalog() {
		// Give each box numeric value bassed on items array.
		$this->_evaluatedCatalog = array();
		foreach ($this->_catalog as $boxId => $items) {
			$cv = 0;
			foreach ($items as $itemId => $quantity) {
				if(isset($this->_items[$itemId]) && $quantity <= $this->_items[$itemId]) {
					$cv -= $quantity;
				}
				else if(isset($this->_items[$itemId]) && $quantity > $this->_items[$itemId]) {
					$cv -= $this->_items[$itemId];
				}
			}
			$this->_evaluatedCatalog[$boxId] = $cv;
		}
		//print_r($this->_evaluatedCatalog);

	}

	private function _subtractBox($boxId) {
		// Subtract items.
		foreach ($this->_catalog[$boxId] as $itemId => $quantity) {
			if(isset($this->_items[$itemId])) {
				$this->_items[$itemId] -= $quantity;
				if($this->_items[$itemId] <= 0) {
					unset($this->_items[$itemId]);
				}
			}
		}

		// Add box to final array.
		if(!isset($this->_requiredBoxes[$boxId])) {
			$this->_requiredBoxes[$boxId] = array(0, array_keys($this->_catalog[$boxId]));
		}

		$this->_requiredBoxes[$boxId][0] += +1;
	}

	public function __construct($items, $catalog) {
		// Prepare/filter items.
		$this->_items = array();
		foreach ($items as $itemId => $neededQuantity) {
			$this->_items[$itemId] = intval($neededQuantity);
		}

		// Prepare/filter catalog.
		$this->_catalog = array();
		foreach ($catalog as $boxId => $items) {
			$this->_catalog[$boxId] = array();
			foreach ($items as $itemId => $quantity) {
				$this->_catalog[$boxId][$itemId] = intval($quantity);
			}
		}
	}

	public function getRequiredBoxes() {
		$requiredItems = array_sum($this->_items);
		$this->_requiredBoxes = array();
		while ($requiredItems > 0) {
			$this->_evaluateCatalog();
			$sv = min($this->_evaluatedCatalog);
			$boxId = array_search($sv, $this->_evaluatedCatalog);
			$this->_subtractBox($boxId);
			$requiredItems = array_sum($this->_items);
		}
		return $this->_requiredBoxes;
	}

}

$o = new RequiredBoxes($items, $boxes);
$rb = $o->getRequiredBoxes();
foreach ($rb as $boxId => $bs) {
	print 'Box: ' . $boxId . ' Quantity: ' . $bs[0] . '<br/>';
	print 'Items: ';
	foreach ($bs[1] as $v) {
		print $v . ',';
	}
	print '<br/><br/>';
}
